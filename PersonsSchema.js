const mongoose = require("mongoose");

const PersonSchema = mongoose.Schema({
    mobModelNumber: String, 
    email_id:String,
    tn_medium:String,
    mobID:String,
    className:String,
    mob_no:String,
    mobSdkNumber: String, 
    mobBoardName:String,
    mobManufacturerName:String,
    mobVersionCode:String,
    mobRAM:String,
    name:String,
    location:String,
    board:String,
    mobBrandName:String,
    created_date:String,
    updated_date:String 
});

module.exports = mongoose.model('usersRegisterCol', PersonSchema);