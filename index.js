const express = require("express");
const app = express();
const morgan = require("morgan");
const mongoose = require("mongoose");
// const uuid = require("uuid");
require("dotenv/config");

// Body-parser
app.use(express.json());

// Middleware
app.use(morgan("dev"));

// Router 
const Personrouter = require("./PersonsRoute");
app.use('/usersRegisterCol',Personrouter);

// const myPersons = [
//     {
//         id : uuid.v4(),
//         name : "Rajan",
//         age : 50
//     },
//     {
//         id : uuid.v4(),
//         name : "Edword",
//         age : 55
//     },
//     {
//         id : uuid.v4(),
//         name : "Andrews",
//         age : 57
//     },
// ]

// GetAllPersons
// app.get('/',(req,res) => {
//     res.status(200).json(myPersons);
// });

// // GetById
// app.get('/:id',async(req,res) => {
//     // res.json(req.params.id);
//     const getOne = await myPersons .filter(e => e.id === req.params.id)
//     res.status(200).json(getOne)
// });

// // PostPersons
// app.post('/',async(req,res) => {
//     // console.log(req.body);
//     myPersons.push(req.body);
//     res.status(200).json(req.body); 
// });

// LocalHost
// app.listen(2000,() => {
//     console.log(`Server Started On 2000`);
// });
const PORT = process.env.PORT || 2000
app.listen(PORT,() => {
    console.log(`Server Started On ${PORT}`);
});

// DB SERVER CREATION
mongoose.set('strictQuery', false);
mongoose.set('strictQuery', true);
mongoose.connect(process.env.MYDB_CONNECTION, (err) => {
    if(err){console.log('DB Connected Connected!');}
    console.log('DB Connected Successfully!');
});
