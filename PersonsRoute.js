
const express = require("express");
const router = express.Router();
const usersRegisterCol = require("./PersonsSchema");

// PostPersons
// router.post("/",async(req,res) => {
//     // res.json("Router is Working");
//     // console.log(req.body.Name);
//     try {
//     const postPerson = await new Persons({
//         Name : req.body.Name,
//         Age : req.body.Age
//     });
//     const savePersons = await postPerson.save();
//     res.status(200).json(savePersons);
//     }
//     catch(err){
//         res.json({"err": err})
//     }
// });

// GetPersons
router.get("/",async(req,res) => {
    try {
        const getAll = await usersRegisterCol.find();
        console.log(getAll);
        res.status(200).json(getAll);
    }
    catch(err){
        res.json({"err":err})
    }
});

// GetById
// router.get("/:id",async(req,res) => {
//     try{
//         const getById = await Persons.findById(req.params.id);
//         res.status(200).json(getById);
//     }
//     catch(err) {
//         res.json({"err": err})
//     }
// });

// UpdatePersons
// router.put("/:id",async(req,res) => {
//     try{
//         console.log(req.params.id);
//         const updPersons = await Persons.updateOne({ _id:req.params.id},{$set:{Name:req.body.Name,Age:req.body.Age}})
//         res.status(200).json(updPersons);
//     }
//     catch(err){
//         res.json({"err":err})
//     }
// });

// DeletePersons
// router.delete("/:id",async(req,res) => {
//     try{
//         const delPersons = await Persons.remove({_id: req.params.id});
//         res.status(200).json(delPersons);
//     }
//     catch(err){
//         res.status({"err":err})
//     }
// })

module.exports = router;